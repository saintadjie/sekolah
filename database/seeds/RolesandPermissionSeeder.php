<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesandPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions    
        // Master Data
        Permission::create(['name' => 'Melihat daftar guru']);
        Permission::create(['name' => 'Menambah data guru']);
        Permission::create(['name' => 'Mengubah data guru']);
        Permission::create(['name' => 'Melihat daftar eskul']);
        Permission::create(['name' => 'Menambah data eskul']);
        Permission::create(['name' => 'Mengubah data eskul']);
        Permission::create(['name' => 'Melihat daftar kelas']);
        Permission::create(['name' => 'Menambah data kelas']);
        Permission::create(['name' => 'Mengubah data kelas']);
        Permission::create(['name' => 'Melihat daftar murid']);
        Permission::create(['name' => 'Menambah data murid']);
        Permission::create(['name' => 'Mengubah data murid']);
        Permission::create(['name' => 'Melihat daftar kategori asset']);
        Permission::create(['name' => 'Menambah data kategori asset']);
        Permission::create(['name' => 'Mengubah data kategori asset']);
        Permission::create(['name' => 'Melihat daftar jenis asset']);
        Permission::create(['name' => 'Menambah data jenis asset']);
        Permission::create(['name' => 'Mengubah data jenis asset']);
        Permission::create(['name' => 'Melihat daftar satker']);
        Permission::create(['name' => 'Menambah data satker']);
        Permission::create(['name' => 'Mengubah data satker']);
        Permission::create(['name' => 'Melihat daftar penyedia']);
        Permission::create(['name' => 'Menambah data penyedia']);
        Permission::create(['name' => 'Mengubah data penyedia']);

        // Transaksi

        Permission::create(['name' => 'Melihat daftar jamaah']);

        // Laporan

		Permission::create(['name' => 'Mencetak seluruh Laporan']);
        Permission::create(['name' => 'Mencetak Laporan berdasarkan kantor']);

        // create roles and assign existing permissions
        
        $role = Role::create(['name' => 'SUPERADMIN']);
        $role->givePermissionTo([	'Melihat daftar guru', 
        							'Menambah data guru', 
        							'Mengubah data guru',
        							'Melihat daftar eskul',
        							'Menambah data eskul',
							        'Mengubah data eskul',
							        'Melihat daftar kelas',
							        'Menambah data kelas',
							        'Mengubah data kelas',
							        'Melihat daftar murid',
							        'Menambah data murid',
							        'Mengubah data murid',
                                    'Melihat daftar kategori asset',
                                    'Menambah data kategori asset',
                                    'Mengubah data kategori asset',
                                    'Melihat daftar jenis asset',
                                    'Menambah data jenis asset',
                                    'Mengubah data jenis asset',
                                    'Melihat daftar satker',
                                    'Menambah data satker',
                                    'Mengubah data satker',
                                    'Melihat daftar penyedia',
                                    'Menambah data penyedia',
                                    'Mengubah data penyedia',

        						]);

       	$role = Role::create(['name' => 'ADMIN']);
        $role->givePermissionTo([	'Melihat daftar guru', 
        							'Menambah data guru', 
        							'Mengubah data guru',
        							'Melihat daftar eskul',
        							'Menambah data eskul',
							        'Mengubah data eskul',
							        'Melihat daftar kelas',
							        'Menambah data kelas',
							        'Mengubah data kelas',
        						]);

        $role = Role::create(['name' => 'MEMBER']);
        $role->givePermissionTo([	'Melihat daftar kelas',
        							'Menambah data kelas',
							        'Mengubah data kelas',
        						]);
    }
}
