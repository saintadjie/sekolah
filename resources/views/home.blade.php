@extends('layouts.layout')
@section('content')

<!-- <div class="block-header">
    <h2>BERANDA</h2>
</div> -->

<!-- Widgets -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-4 col-sm-6 col-xs-12">
        <div class="card">
            <div class="body bg-red">
                Selamat Datang {{ Auth::user()->nama }} - {{ Auth::user()->nip }}.
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-zoom-effect">
            <div class="icon">
                <i class="material-icons">face</i>
            </div>
            <div class="content">
                <div class="text">Pengguna Aktif</div>
                <div class="number count-to" data-from="0" data-to="{{$userLogin}}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-cyan hover-zoom-effect">
            <div class="icon">
                <i class="material-icons">domain</i>
            </div>
            <div class="content">
                <div class="text">Lokasi</div>
                <div class="number count-to" data-from="0" data-to="{{$counteskul}}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-light-green hover-zoom-effect">
            <div class="icon">
                <i class="material-icons">people</i>
            </div>
            <div class="content">
                <div class="text">Jenis Kontainer</div>
                <div class="number count-to" data-from="0" data-to="{{$userLogin}}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-orange hover-zoom-effect">
            <div class="icon">
                <i class="material-icons">airplanemode_active</i>
            </div>
            <div class="content">
                <div class="text">Kontainer</div>
                <div class="number count-to" data-from="0" data-to="{{$userLogin}}" data-speed="15" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-purple hover-zoom-effect">
            <div class="icon">
                <i class="material-icons">store</i>
            </div>
            <div class="content">
                <div class="text">Posisi</div>
                <div class="number count-to" data-from="0" data-to="{{$userLogin}}" data-speed="15" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>

    @push('script-footer')
    <script type="text/javascript">
        $('.count-to').countTo()
    </script>
    @endpush
</div>
<!-- #END# Widgets -->

@endsection