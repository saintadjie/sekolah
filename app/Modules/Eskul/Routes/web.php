<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'eskul', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'eskul'], function () {

    	Route::get('/', 'EskulController@index')->middleware('permission:Melihat daftar eskul');
        Route::get('add', 'EskulController@page_add')->middleware('permission:Menambah data eskul');
        Route::get('show/{id}', 'EskulController@page_show')->middleware('permission:Mengubah data eskul');

    });

});
