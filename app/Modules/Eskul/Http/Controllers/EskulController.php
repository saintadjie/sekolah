<?php

namespace App\Modules\Eskul\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\model\eskul\Eskul;
use App\model\murid\MuridEskul;

class EskulController extends Controller
{
    public function index() {
    	$rs         = Eskul::select('id', 'kode_eskul', 'nama_eskul', 'keterangan')
                      	->get();
         
        return view('eskul::index', ['rs' => $rs]);

    }

    public function page_add(Request $request) {
		$rs         = Eskul::select('id', 'kode_eskul', 'nama_eskul', 'keterangan')
                      	->get();
        
        return view('eskul::add', ['rs' => $rs]);
	}

	public function store(Request $request) {
		$check 		= Eskul::where('kode_eskul', $request->kode_eskul)->first();

		if (!empty($check)) {

			return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );

		} else {

            $eskul 				= new Eskul();
			$eskul->kode_eskul 	= $request->kode_eskul;
			$eskul->nama_eskul 	= $request->nama_eskul;
			$eskul->keterangan 	= $request->keterangan;
			$eskul->save();

			return response()->json(['status' => 'OK']);
		}
	}

	public function page_show($id) {

        $rs         = Eskul::findOrfail($id);

        return view('eskul::show', ['rs' => $rs]);
    }

    public function edit(Request $request) {

        $kode_eskul 		= $request->kode_eskul;             
        $kode_eskullama 	= $request->kode_eskullama;

        if ($kode_eskullama   != $kode_eskul) {
            $check = Eskul::where('kode_eskul', $request->kode_eskul)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
        }

        $eskul 				= Eskul::find($request->id);
        $eskul->kode_eskul 	= $request->kode_eskul;
        $eskul->nama_eskul 	= $request->nama_eskul;
        $eskul->keterangan 	= $request->keterangan;
        $eskul->save();

        MuridEskul::where('kode_eskul', $kode_eskullama)
            ->update(['kode_eskul' => $kode_eskul]);

        return response()->json(['status' => 'OK']);
    }

	public function delete($id) {

        $eskul 	= Eskul::findOrfail($id);
        $eskul->delete();

        return response()->json(['status' => 'OK']);

    }
}
