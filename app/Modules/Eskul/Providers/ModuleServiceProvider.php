<?php

namespace App\Modules\Eskul\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('eskul', 'Resources/Lang', 'app'), 'eskul');
        $this->loadViewsFrom(module_path('eskul', 'Resources/Views', 'app'), 'eskul');
        $this->loadMigrationsFrom(module_path('eskul', 'Database/Migrations', 'app'), 'eskul');
        $this->loadConfigsFrom(module_path('eskul', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('eskul', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
