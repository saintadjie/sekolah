<?php

namespace App\Modules\Kelas\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('kelas', 'Resources/Lang', 'app'), 'kelas');
        $this->loadViewsFrom(module_path('kelas', 'Resources/Views', 'app'), 'kelas');
        $this->loadMigrationsFrom(module_path('kelas', 'Database/Migrations', 'app'), 'kelas');
        $this->loadConfigsFrom(module_path('kelas', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('kelas', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
