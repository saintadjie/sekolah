<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsKelasGuruTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_kelas_guru', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_kelas');
            $table->string('kode_guru');
            $table->foreign('kode_kelas')->references('kode_kelas')->on('ms_kelas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_kelas_guru');
    }
}
