<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsMuridTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_murid', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_murid')->unique();
            $table->string('nama_murid');
            $table->string('jenis_kelamin');
            $table->string('kelas_1');
            $table->string('kelas_2');
            $table->string('kelas_3');
            $table->string('alamat')->nullable();
            $table->string('photo')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_murid');
    }
}
