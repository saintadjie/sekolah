<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsMuridEskulTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_murid_eskul', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_murid');
            $table->string('kode_eskul');
            $table->foreign('kode_murid')->references('kode_murid')->on('ms_murid')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_murid_eskul');
    }
}
