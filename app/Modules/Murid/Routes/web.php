<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'murid', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'murid'], function () {

    	Route::get('/', 'MuridController@index')->middleware('permission:Melihat daftar murid');
        Route::get('add', 'MuridController@page_add')->middleware('permission:Menambah data murid');
        Route::get('show/{id}', 'MuridController@page_show')->middleware('permission:Mengubah data murid');

    });

});