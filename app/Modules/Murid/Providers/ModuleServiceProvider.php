<?php

namespace App\Modules\Murid\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('murid', 'Resources/Lang', 'app'), 'murid');
        $this->loadViewsFrom(module_path('murid', 'Resources/Views', 'app'), 'murid');
        $this->loadMigrationsFrom(module_path('murid', 'Database/Migrations', 'app'), 'murid');
        $this->loadConfigsFrom(module_path('murid', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('murid', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
