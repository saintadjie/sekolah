<?php

namespace App\Modules\Guru\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('guru', 'Resources/Lang', 'app'), 'guru');
        $this->loadViewsFrom(module_path('guru', 'Resources/Views', 'app'), 'guru');
        $this->loadMigrationsFrom(module_path('guru', 'Database/Migrations', 'app'), 'guru');
        $this->loadConfigsFrom(module_path('guru', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('guru', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
