<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'guru', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'guru'], function () {

    	Route::get('/', 'GuruController@index')->middleware('permission:Melihat daftar guru');
        Route::get('add', 'GuruController@page_add')->middleware('permission:Menambah data guru');
        Route::get('show/{id}', 'GuruController@page_show')->middleware('permission:Mengubah data guru');

    });

});
