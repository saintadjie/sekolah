<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/guru', 'middleware' => 'auth:api'], function () {

	Route::group(['prefix' => 'guru'], function () {
    	Route::post('store', 'GuruController@store');
    	Route::post('edit', 'GuruController@edit');
    	Route::post('delete/{id}', 'GuruController@delete');
    });
	
});