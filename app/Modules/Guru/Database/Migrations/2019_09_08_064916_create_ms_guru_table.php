<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsGuruTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_guru', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_guru')->unique();
            $table->string('nama_guru');
            $table->string('jenis_kelamin');
            $table->string('alamat')->nullable();
            $table->string('photo')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_guru');
    }
}
