<?php

namespace App\Modules\Guru\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\model\guru\Guru;
use App\model\kelas\KelasGuru;

use DB;
use Hash;
use Image;
use File;
use Auth;

class GuruController extends Controller
{
    public function index() {
    	$rs         = Guru::select('id', 'kode_guru', 'nama_guru', 'jenis_kelamin', 'alamat', 'photo', 'keterangan')
                      	->get();
         
        return view('guru::index', ['rs' => $rs]);

    }

    public function page_add(Request $request) {
		$rs         = Guru::select('id', 'kode_guru', 'nama_guru', 'jenis_kelamin', 'alamat', 'photo', 'keterangan')
                      	->get();
        
        return view('guru::add', ['rs' => $rs]);
	}

	public function store(Request $request) {
		$path 		= null;
		$check 		= Guru::where('kode_guru', $request->kode_guru)->first();

		if (!empty($check)) {

			return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );

		} 

		if ($request->hasFile('photo')) {
			$dir = public_path() . "/files/foto_guru/$request->kode_guru";
			if (!is_dir($dir)) {
				File::makeDirectory($dir, $mode = 0777, true, true);         
			}
			Image::make($request->file('photo'))->resize(150, 150)->save($dir . '/' . $request->kode_guru . '.jpg', 100);
			$path = 'files/foto_guru/' . $request->kode_guru . '/' . $request->kode_guru . '.jpg';
		}

        $guru 					= new Guru();
		$guru->kode_guru 		= $request->kode_guru;
		$guru->nama_guru 		= $request->nama_guru;
		$guru->jenis_kelamin 	= $request->jenis_kelamin;
		$guru->alamat 			= $request->alamat;
		$guru->photo 			= $path != null ? $path : null; 
		$guru->keterangan 		= $request->keterangan;
		$guru->save();

		return response()->json(['status' => 'OK']);
	}

	public function page_show($id) {

        $rs         = Guru::findOrfail($id);

        return view('guru::show', ['rs' => $rs]);
    }

    public function edit(Request $request) {

        $kode_guru 		= $request->kode_guru;             
        $kode_gurulama 	= $request->kode_gurulama;

        if ($kode_gurulama   != $kode_guru) {
            $check = Guru::where('kode_guru', $request->kode_guru)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
        }

        if ($request->hasFile('photo')) {
            $dir = public_path() . "/files/foto_guru/$request->kode_guru";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }
            Image::make($request->file('photo'))->resize(150, 150)->save($dir . '/' . $request->kode_guru . '.jpg', 100);
            $path = 'files/foto_guru/' . $request->kode_guru . '/' . $request->kode_guru . '.jpg';
        }

        $guru 					= Guru::find($request->id);
        $guru->kode_guru 		= $request->kode_guru;
		$guru->nama_guru 		= $request->nama_guru;
		$guru->jenis_kelamin 	= $request->jenis_kelamin;
		$guru->alamat 			= $request->alamat;
		$guru->photo 			= $path != null ? $path : null; 
		$guru->keterangan 		= $request->keterangan;
        $guru->save();

        KelasGuru::where('kode_guru', $kode_gurulama)
            ->update(['kode_guru' => $kode_guru]);

        return response()->json(['status' => 'OK']);
    }

	public function delete($id) {

        $guru 	= Guru::findOrfail($id);
        $guru->delete();

        return response()->json(['status' => 'OK']);

    }
}
