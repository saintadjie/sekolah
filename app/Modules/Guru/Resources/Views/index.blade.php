@extends('layouts.layout')
@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ol class="breadcrumb breadcrumb-bg-indigo">
            <li><a href="{{url('/home')}}"><i class="material-icons">home</i> Home</a></li>
            <li class="active"><i class="material-icons">record_voice_over</i> Guru</li>
        </ol>
		<div class="card">
			<div class="body">
				<div>
					<a href="{{url('/guru/guru/add')}}" id="btn_tambah" class="btn bg-blue waves-effect"><i class="material-icons">add_circle_outline</i>&nbsp;Tambah Data</a>
				</div>
				<hr>
				<div class="panel panel-success">
					<div class="panel-heading bg-indigo">
						Daftar Guru
					</div>
					<div class="panel-body table-responsive">
						<table id="tb_guru" width="100%" role="grid" class="table table-striped table-bordered table-hover table-responsive">
							<thead class="breadcrumb-bg-blue">
								<tr>
									<th style="text-align: center; color: #fff" class="th_table">Kode Guru</th>
									<th style="text-align: center; color: #fff" class="th_table">Nama Guru</th>
									<th style="text-align: center; color: #fff" class="th_table">Alamat</th>
									<th style="text-align: center; color: #fff" class="th_table">Keterangan</th>
									<th style="text-align: center; color: #fff" class="th_table">Aksi</th>
								</tr>
							</thead>
							<tbody id="tbody">
								
								@foreach($rs as $result)
		                        <tr id="{{$result->id}}">
									<td style="text-align: center;">{{ $result->kode_guru }}</td>
		                            <td style="text-align: center;">{{ $result->nama_guru }}</td>
		                            <td style="text-align: center;">{{ $result->alamat }}</td>
		                            <td style="text-align: center;">{{ $result->keterangan }}</td>
		                            <td style="text-align: center;">
										<a href="{{url('/guru/guru/show/'.$result->id)}}" title="EDIT Guru">
											<i class="btn btn-xs waves-effect material-icons" id="btn_edit">edit</i>
										</a>
										<i class="btn btn-xs waves-effect material-icons" id="btn_hapus" title="Hapus Data" data-kodeguru="{{$result->kode_guru}}">delete</i>
									</td>
								</tr>
		                        @endforeach

							</tbody>
							
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>

</div>
@push('script-footer')
<script src="{{url('js/guru/index_app.js')}}"></script>

<script type="text/javascript">
	var url_api = "{{url('api/v1/guru/guru/delete')}}"
	var url_guru = "{{url('/guru/guru')}}"
</script>
@endpush
@endsection