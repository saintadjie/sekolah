@extends('layouts.layout')
@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ol class="breadcrumb breadcrumb-bg-indigo">
            <li><a href="{{url('/home')}}"><i class="material-icons">home</i> Home</a></li>
            <li><a href="{{url('/guru/guru')}}"><i class="material-icons">record_voice_over</i> Guru</a></li>
            <li class="active"><i class="material-icons">library_add</i> Tambah Guru</li>
        </ol>
		<div class="card">
			<div class="header bg-blue">
				<h2>
					<u>Guru</u><small>Menambahkan Data Guru</small>
				</h2>
			</div>

			<div class="body">
				<div class="row clearfix">

					<div class="col-md-12">
						<form id="form_guru">
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
								
								<br/>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="kode_guru" name="kode_guru">
										<label class="form-label">Kode Guru</label>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="nama_guru" name="nama_guru">
										<label class="form-label">Nama Guru</label>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-12">
										<label class="form-label">Jenis Kelamin</label>
										<div class="demo-radio-button">
											<input name="jenis_kelamin" type="radio" id="jkp" value="Pria" class="with-gap radio-col-indigo" />
			                                <label for="jkp">Pria</label>
			                                <input name="jenis_kelamin" type="radio" id="jkw" value="Wanita" class="with-gap radio-col-pink" />
			                                <label for="jkw">Wanita</label>
			                            </div>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<textarea rows="4" class="form-control no-resize"  id="alamat" name="alamat"></textarea>
										<label class="form-label">Alamat</label>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<textarea rows="4" class="form-control no-resize"  id="keterangan" name="keterangan"></textarea>
										<label class="form-label">Keterangan</label>
									</div>
								</div>

								<label for="userpic">Foto</label>
								<div class="row clearfix">
									<div class="col-md-12">
										<div class="file-path-wrapper">
											<input type="file" name="photo" id="photo" class="form-control"><i> Diupload dengan format PNG, JPG Max 500 Kb </i>
										</div>
									</div>
								</div>
								<br/>

								<div class="row clearfix">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="button" class="btn bg-cyan waves-effect" id="btn_simpan"><i class="material-icons">save</i><span>&nbsp;Simpan Data</span></button>
											<button type="button" class="btn bg-orange waves-effect" id="btn_batal"><i class="material-icons">clear</i><span>&nbsp;Batal</span></button>
										</div>
									</div>
								</div>

							</div>	

							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>		

						</form>	
					</div>

				</div>
			</div>
			
		</div>
	</div>

</div>
@push('script-footer')
<script src="{{url('js/guru/add_app.js')}}"></script>

<script type="text/javascript">
	var url_api = "{{url('api/v1/guru/guru/store')}}"
</script>

@endpush
@endsection

