@extends('layouts.layout')
@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <ol class="breadcrumb breadcrumb-bg-indigo">
            <li><a href="{{url('/home')}}"><i class="material-icons">home</i> Home</a></li>
            <li><a href="{{url('/guru/guru')}}"><i class="material-icons">record_voice_over</i> Guru</a></li>
            <li class="active"><i class="material-icons">rate_review</i> Ubah Guru</li>
        </ol>
		<div class="card">
			<div class="header bg-blue">
				<h2>
					<u>Guru</u><small>Mengubah Data Guru</small>
				</h2>
			</div>

			<div class="body">
				<div class="row clearfix">

					<div class="col-md-12">
						<form id="form_guru">
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
								<br>
								<div class="row">
									<div class="col-md-12">
										<center>
											@if($rs->photo != null)
												<img src="{{url($rs->photo)}}" class="img-circle img-responsive thumbnail" alt="User Image" id="user-img" height="240" width="240">
											@else
												<img src="{{url('assets\template\img\user.png')}}" class="img-circle img-responsive thumbnail" alt="User Image" id="user-img" height="240" width="240">
											@endif
										</center>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="kode_guru" name="kode_guru" value="{{$rs->kode_guru}}" disabled>
										<label class="form-label">Kode Guru</label>
									</div>
								</div>

								<div class="form-group form-float" hidden>
									<div class="form-line">
										<input type="text" class="form-control" id="kode_gurulama" name="kode_gurulama" value="{{$rs->kode_guru}}" disabled>
										<label class="form-label">Kode Guru</label>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="nama_guru" name="nama_guru" value="{{$rs->nama_guru}}" disabled>
										<label class="form-label">Nama Guru</label>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-12">
										<label class="form-label">Jenis Kelamin</label>
										<div class="demo-radio-button">
											<input name="jenis_kelamin" type="radio" id="jkp" class="with-gap radio-col-indigo" value="Pria" disabled @if($rs->jenis_kelamin == 'Pria') checked @endif/>
			                                <label for="jkp">Pria</label>
			                                <input name="jenis_kelamin" type="radio" id="jkw" class="with-gap radio-col-pink" value="Wanita" disabled @if($rs->jenis_kelamin == 'Wanita') checked @endif/>
			                                <label for="jkw">Wanita</label>
			                            </div>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<textarea rows="4" class="form-control no-resize"  id="alamat" name="alamat" disabled>{{$rs->alamat}}</textarea>
										<label class="form-label">Alamat</label>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<textarea rows="4" class="form-control no-resize"  id="keterangan" name="keterangan" disabled>{{$rs->keterangan}}</textarea>
										<label class="form-label">Keterangan</label>
									</div>
								</div>

								<label for="userpic">Foto</label>
								<div class="row clearfix">
									<div class="col-md-12">
										<div class="file-path-wrapper">
											<input type="file" name="photo" id="photo" class="form-control" disabled><i> Diupload dengan format PNG, JPG Max 500 Kb </i>
										</div>
									</div>
								</div>
								<br/>

								<div class="row clearfix" id="hidden_ubah">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="button" class="btn bg-cyan waves-effect" id="btn_ubah"><i class="material-icons">edit</i>&nbsp;Ubah Data</button>
										</div>
									</div>
								</div>

								<div class="row clearfix" hidden="true" id="btn_hidden">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="button" class="btn bg-cyan waves-effect" id="btn_simpan"><i class="material-icons">save</i>&nbsp;Simpan Data</button>
											<button type="button" class="btn bg-orange waves-effect" id="btn_batal"><i class="material-icons">clear</i>&nbsp;Batal</button>
										</div>
									</div>
								</div>

							</div>	

							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>		

						</form>	
					</div>

				</div>
			</div>
			
		</div>
	</div>

</div>
@push('script-footer')
<script src="{{url('js/guru/show_app.js')}}"></script>

<script type="text/javascript">
	var id = "{{$rs->id}}"
	var url_api = "{{url('api/v1/guru/guru/edit')}}"
	var url_guru = "{{url('/guru/guru/')}}"

</script>
@endpush
@endsection

