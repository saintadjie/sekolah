<?php

namespace App\Modules\Petugas\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('petugas', 'Resources/Lang', 'app'), 'petugas');
        $this->loadViewsFrom(module_path('petugas', 'Resources/Views', 'app'), 'petugas');
        $this->loadMigrationsFrom(module_path('petugas', 'Database/Migrations', 'app'), 'petugas');
        $this->loadConfigsFrom(module_path('petugas', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('petugas', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
