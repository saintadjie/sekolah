<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Model\eskul\Eskul;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userLogin          = Auth::user()->id;
        $userKantor         = Auth::user()->kode_kantor;
        $userRole           = Auth::user()->level_pengguna;
        $userNIP            = Auth::user()->nip;

        $counteskul        = Eskul::count();


        return view('home', ['userLogin'=> $userLogin, 'counteskul' => $counteskul]);


    }
}
