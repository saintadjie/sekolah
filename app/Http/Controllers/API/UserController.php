<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{

    public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['nip' => request('nip'), 'password' => request('password')])){
            $user = Auth::user();
            $ch = curl_init('http://10.0.30.35/passporttools/API/getOfficeBySearch');  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['request' => json_encode(['username' => 'bcm', 'password' => 'bcmApi!2018', 'searchparam' => 'type','searchvalue' => 'kanim'])]));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); 
            $exec = curl_exec($ch);
            $rs = json_decode($exec)->data;


            foreach ($rs as $key => $value) {
                $data = array_search($value->officeid, array_column($rs, 'officeid'));
                $value->kode_kantor = $rs[$data]->officeid;
                $value->nama_kantor = $rs[$data]->name;
                $value->type_kantor = $rs[$data]->type;
            }

            $success['token'] =  $user->createToken('nApp')->accessToken;
            $success['nip'] =  $user->nip;
            $success['nama'] =  $user->nama;
            $success['kode_kantor'] =  $user->kode_kantor;
            $success['nama_kantor'] =  $rs[$data]->name;
            $success['status'] =  true;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{

            $error['status'] =  false;
            return response()->json(['error'=> $error], 401);

        }
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function cek() {
        // get logged-in user
        $user = auth()->user();
        $user->givePermissionTo('Melihat daftar lokasi');

        // get all inherited permissions for that user
        $permissions = $user->getAllPermissions();

        return response()->json([
            'b' => $permissions
        ], 200);
    }
}