<?php

namespace App\Model\Kelas;

use Illuminate\Database\Eloquent\Model;

class KelasGuru extends Model
{
    protected $table = "ms_kelas_guru";
    
    protected $fillable = ['kode_kelas', 'kode_guru'];
}
