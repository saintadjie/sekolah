<?php

namespace App\Model\Kelas;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = "ms_kelas";
    
    protected $fillable = ['kode_kelas', 'nama_kelas', 'keterangan'];
}
