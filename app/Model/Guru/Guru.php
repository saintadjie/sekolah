<?php

namespace App\Model\Guru;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = "ms_guru";
    
    protected $fillable = ['kode_guru', 'nama_guru', 'jenis_kelamin', 'alamat', 'photo', 'keterangan'];
}
