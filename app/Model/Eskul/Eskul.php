<?php

namespace App\Model\Eskul;

use Illuminate\Database\Eloquent\Model;

class Eskul extends Model
{
    protected $table = "ms_eskul";
    
    protected $fillable = ['kode_eskul', 'nama_eskul', 'keterangan'];
}
