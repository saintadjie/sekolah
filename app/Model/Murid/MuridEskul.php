<?php

namespace App\Model\Murid;

use Illuminate\Database\Eloquent\Model;

class MuridEskul extends Model
{
    protected $table = "ms_murid_eskul";
    
    protected $fillable = ['kode_murid', 'kode_eskul'];
}
