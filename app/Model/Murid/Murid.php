<?php

namespace App\Model\Murid;

use Illuminate\Database\Eloquent\Model;

class Murid extends Model
{
    protected $table = "ms_murid";
    
    protected $fillable = ['kode_murid', 'nama_murid', 'jenis_kelamin', 'kelas_1', 'kelas_2', 'kelas_3', 'alamat', 'photo', 'keterangan'];
}
