$(function () {

	var kode_eskullama = "{{$result->kode_eskul}}";

	$('#btn_ubah').on('click', function(){
		$("#btn_hidden").attr("hidden","false").removeAttr("hidden")
		$("input:disabled").removeAttr("disabled")
        $("textarea:disabled").removeAttr("disabled")
		$('#hidden_ubah').prop('hidden', 'true')
	})

    $('#btn_batal').click(function() {
        location.reload();
    });
    
	$('#btn_simpan').click(function(){

        if ($('#kode_eskul').val() == '') {
            swal( "Kesalahan", "Kode Ekstrakurikuler tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_eskul').val() == '') {
            swal( "Kesalahan", "Nama Ekstrakurikuler tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_eskul")[0])
        uploadfile.append('id', id)
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        location.href = url_eskul
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Kesalahan", "Ekstrakurikuler dengan kode: " + $('#kode_eskul').val() + " telah ada", "error")
                }
            }
        })
    })

})