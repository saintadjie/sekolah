$(function () {
    
    $('#btn_batal').click(function(){
        $('#kode_eskul').val('')
        $('#nama_eskul').val('')
        $('#keterangan').val('')
    })

    $('#btn_simpan').click(function(){
        if ($('#kode_eskul').val() == '') {
            swal( "Kesalahan", "Kode Ekstrakurikuler tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_eskul').val() == '') {
            swal( "Kesalahan", "Nama Ekstrakurikuler tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_eskul")[0])
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        $('#kode_eskul').val('')
                        $('#nama_eskul').val('')
                        $('#keterangan').val('')
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Error", "Ekstrakurikuler sudah ada", "error")
                }
            }
        })
    })

})
