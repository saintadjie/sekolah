$(function () {

    $('#photo').fileinput({
        showUpload: false,
        allowedFileExtensions: ['jpeg', 'jpg', 'png']
    })

	var kode_gurulama = "{{$result->kode_guru}}";

	$('#btn_ubah').on('click', function(){
		$("#btn_hidden").attr("hidden","false").removeAttr("hidden")
		$("input:disabled").removeAttr("disabled")
        $("textarea:disabled").removeAttr("disabled")
        $("#photo").fileinput("enable").fileinput("refresh", {showUpload: false});
		$('#hidden_ubah').prop('hidden', 'true')
	})

    $('#btn_batal').click(function() {
        location.reload();
    });
    
	$('#btn_simpan').click(function(){

        if ($('#kode_guru').val() == '') {
            swal( "Kesalahan", "Kode Guru tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_guru').val() == '') {
            swal( "Kesalahan", "Nama Guru tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_guru")[0])
        uploadfile.append('id', id)
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        location.href = url_guru
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Kesalahan", "Guru dengan kode: " + $('#kode_guru').val() + " telah ada", "error")
                }
            }
        })
    })

})