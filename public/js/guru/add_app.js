$(function () {

    $('#photo').fileinput({
        showUpload: false,
        allowedFileExtensions: ['jpeg', 'jpg', 'png']
    })
    
    $('#btn_batal').click(function(){
        $('#kode_guru').val('')
        $('#nama_guru').val('')
        $('input[name="jenis_kelamin"]').prop('checked', false)
        $('#alamat').val('')
        $('input[type="file"]').fileinput('clear')
        $('#keterangan').val('')
    })

    $('#btn_simpan').click(function(){
        if ($('#kode_guru').val() == '') {
            swal( "Kesalahan", "Kode Guru tidak boleh kosong", "error" )
            return
        } else if ($('#nama_guru').val() == '') {
            swal( "Kesalahan", "Nama Guru tidak boleh kosong", "error" )
            return
        } else if (!$('input[name="jenis_kelamin"]').is(':checked')) {
          swal( "Kesalahan", "Silahkan pilih Jenis Kelamin terlebih dahulu", "error" );
          return false;
        }

        var uploadfile = new FormData($("#form_guru")[0])
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        $('#kode_guru').val('')
                        $('#nama_guru').val('')
                        $('input[name="jenis_kelamin"]').prop('checked', false)
                        $('#alamat').val('')
                        $('input[type="file"]').fileinput('clear')
                        $('#keterangan').val('')
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Error", "Guru sudah ada", "error")
                }
            }
        })
    })

})
